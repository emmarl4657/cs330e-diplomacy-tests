# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    def test_read(self):
        s = 'A Madrid Hold\n'
        a = diplomacy_read(s)
        self.assertEqual(a,  ['A', 'Madrid', 'Hold'])

    def test_read_2(self):
        s = 'B Barcelona Move Madrid'
        a = diplomacy_read(s)
        self.assertEqual(a,  ['B', 'Barcelona', 'Move', 'Madrid'])

    def test_read_3(self):
        s = 'C London Support B'
        a = diplomacy_read(s)
        self.assertEqual(a,  ['C', 'London', 'Support', 'B'])

    def test_read_4(self):
        s = 'D Austin Move London'
        a = diplomacy_read(s)
        self.assertEqual(a,  ['D', 'Austin', 'Move', 'London'])

    # ----
    # eval
    # ----
    def test_diplomacy_1(self):
        v = diplomacy_eval([['A',  'Madrid', 'Hold']])
        self.assertEqual(v, ['A Madrid'])

    def test_diplomacy_2(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']])
        self.assertEqual(v, ['A [dead]', 'B Madrid', 'C London'])

    def test_diplomacy_3(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], 
        ['D', 'Austin', 'Move', 'London']])
        self.assertEqual(v, ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]'])

    def test_diplomacy_4(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], 
                                ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']])
        self.assertEqual(v, ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])

    def test_diplomacy_5(self):
        v = diplomacy_eval([['A', 'Madrid', 'Move', 'London'], ['B', 'London', 'Move', 'Madrid']])
        self.assertEqual(v, ['A London', 'B Madrid'])

    def test_diplomacy_6(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'London', 'Move', 'Madrid']])
        self.assertEqual(v, ['A [dead]', 'B [dead]'])

    # -----
    # print
    # -----
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ['A Madrid'])
        self.assertEqual(w.getvalue(), 'A Madrid')

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B Madrid', 'C London'])
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London')
    
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]'])
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]')

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin')

    def test_print_5(self):
        w = StringIO()
        diplomacy_print(w, ['A London', 'B Madrid'])
        self.assertEqual(w.getvalue(), 'A London\nB Madrid')

    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO('A Madrid Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid')

    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London')

    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]')

    def test_solve_4(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin')

    def test_solve_5(self):
        r = StringIO('A Madrid Move London\nB London Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A London\nB Madrid')


# ----
# main
# ----
if __name__ == "__main__":
    main()
